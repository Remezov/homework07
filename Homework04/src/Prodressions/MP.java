package Prodressions;

import java.util.Scanner;

public class MP {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Give me N.");
        int n = s.nextInt();
        int a = 0;
        int d = 2;
        for (int i = 1; i <= n; i++) {
            System.out.printf("%d ", a += (i - 1) * d);
        }
    }
}
