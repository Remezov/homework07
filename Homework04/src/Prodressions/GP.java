package Prodressions;

import java.util.Scanner;

public class GP {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.println("Give me N.");
        int n = s.nextInt();
        int b = 1;
        int q = -2;
        for (int i = 1; i <= n; i++) {
            System.out.printf("%d ", b *= (int)Math.pow(q, i - 1));
        }
    }
}
