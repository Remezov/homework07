package ru.remezov.animal;

public class Employee extends Man {

    public Employee(String name, String employee) {
        super(name, employee);
    }

    public void canWork(){
        System.out.println("I'm " +this.employee+ "!");
    }
}
