package ru.remezov;

import ru.remezov.animal.Dog;
import ru.remezov.animal.Dolphin;
import ru.remezov.animal.Duck;
import ru.remezov.animal.Employee;

public class Main {
    public static void main(String[] args) {

        Dog dog = new Dog("dog");
        dog.getName();
        dog.canRun();

        Duck duck = new Duck("duck");
        duck.getName();
        duck.canFly();
        duck.canRun();
        duck.canSwim();

        Dolphin dolphin = new Dolphin("dolphin");
        dolphin.getName();
        dolphin.canSwim();

        Employee employee1 = new Employee("Ted","runner");
        employee1.getName();
        employee1.canWork();
        employee1.canRun();

        Employee employee2 = new Employee("Vazya", "swimmer");
        employee2.getName();
        employee2.canWork();
        employee2.canSwim();
    }
}
